# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from os import getcwd
from common.nexus import NexusMod
from common.fallout_nv import FalloutNV
from pybuild import InstallDir, File


class Package(NexusMod, FalloutNV):
    NAME = "The Mod Configuration Menu"
    DESC = "Allows any number of mods to be configured from a single menu."
    HOMEPAGE = "https://www.nexusmods.com/newvegas/mods/42507"
    KEYWORDS = "fallout-nv"
    LICENSE = "all-rights-reserved"
    NEXUS_URL = HOMEPAGE
    RDEPENDS = "dev-util/xnvse ui/uio"
    SRC_URI = """
        The_Mod_Configuration_Menu-42507-1-5.7z
        MCM_BugFix_2-42507-.7z
    """
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="The Mod Configuration Menu",
            DOC=["Readme - The Mod Configuration Menu.txt"],
            BLACKLIST=["fomod/*", "The Mod Configuration Menu.esp"],
        ),
        InstallDir(
            ".",
            S="MCM_BugFix_2-42507-",
            PLUGINS=[File("The Mod Configuration Menu.esp")],
        ),
    ]

    def src_unpack(self):
        super().src_unpack()
        self.unpack(
            f"{getcwd()}/The_Mod_Configuration_Menu-42507-1-5/The Mod Configuration Menu.fomod"
        )

    def pkg_postinst(self):
        self.info(
            "MCM won't show up if there aren't any mods with an entry, so don't worry if you don't see MCM in the pause menu."
        )
