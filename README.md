# Fallout: New Vegas

This is the [Portmod](https://gitlab.com/portmod/portmod) package repository for Fallout: New Vegas.

This is experimental and requires that you install on top of an existing installation. See portmod!388.
It also requres the use of a work-in-progress version of bsatool. See https://gitlab.com/OpenMW/openmw/-/merge_requests/783

Eventually you will be able to find details on how to get started on the Portmod [Guide](https://gitlab.com/portmod/portmod/-/wikis/home#guide).
